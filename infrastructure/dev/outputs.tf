output "region" {
  value = module.workflow.region
}

output "cluster_name" {
  value = module.workflow.cluster_name
}

output "oidc_provider_identity" {
  value = module.workflow.oidc_provider_identity
}

output "efs_dns_name" {
  value = module.workflow.efs_dns_name
}

output "efs_ap_id" {
  value = module.workflow.efs_ap_id
}

output "efs_ap_arn" {
  value = module.workflow.efs_ap_arn
}

output "efs_mount_target_ids" {
  value = module.workflow.efs_mount_target_ids
}

output "efs_mount_template" {
  value = module.workflow.efs_mount_template
}