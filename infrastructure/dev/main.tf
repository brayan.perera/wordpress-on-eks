module "workflow" {
  source = "../modules/workflow"
  common_config = var.common_config
  ec2_launch_template = var.ec2_launch_template
  efs = var.efs
  eks_cluster_name = var.eks_cluster_name
  rds = var.rds
  vpc = var.vpc
  eks_cluster_version = var.eks_cluster_version
}