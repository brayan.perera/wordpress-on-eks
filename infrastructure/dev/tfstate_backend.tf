terraform {
  backend "s3" {
    bucket         = "dev-otgs-tf-state"
    key            = "otgs/infra/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "dev-otgs-tf-lock"
    encrypt        = true
  }
}