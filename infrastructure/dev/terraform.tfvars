common_config = {
  aws_account_id = "763511508504"
  region = "us-east-1"
  availability_zones = ["us-east-1a", "us-east-1b", "us-east-1c"]
  environment = "dev"
  name_prefix = "otg"
  git_lab_project_id = "30629592"
}

vpc = {
  vpc_name = "dev-vpc"
  vpc_cidr = "10.0.0.0/16"
  public_subnets_cidr = ["10.0.20.0/24", "10.0.21.0/24", "10.0.22.0/24"]
  private_subnets_cidr = ["10.0.10.0/24", "10.0.11.0/24", "10.0.12.0/24"]
}

rds = {
  instance_name = "otg-dev-mysql"
  instance_class = "db.t3.micro"
  allocated_storage = 5
  storage_type = "gp2"
  engine = "mysql"
  engine_version = "8.0"
  family = "mysql8.0"
  db_name = "otg_wp_db"
  db_user = "otg_wp_user"
  db_pass = "0Tg1PasS123"
  backup_retention_period = 5
  backup_window = "01:00-03:00"
  maintenance_window = "mon:04:00-mon:06:00"
}

efs = {
  name = "otg_wp_efs"
  encrypted = true
  creation_token = "68c166a0-8d8e"
  mount_location = "/otgs/wp_data"
}

ec2_launch_template = {
  name = "otg_wp_eks_nodes_dev"
  image_id = "ami-02e136e904f3da870"
  instance_type = "t2.small"
  ssh_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8mjKN3vbEsaRODGsfepdW02y854csNRum8hNIbYt9LvU5xMRyNKpauWse4EfocKvlVS0139LuxpIJVzT6eCQf9CNzdHyPtXvTCstnkzD4fK7sfZyyljc8Cv1X6RjxiWjnoLkn61uokzApGoVsA+IZsUyZlCHycidvmiLoOS/usmgIxAUEEsKLyvcdchxzygTEeVxTKBdt3yClxVkNi+6lOOpiEinhdTeLTlSbSQPmPzlZRCihvEa8dVvO/hG/O/jZM1M3HCizeTSy6l9xNphg/zbCU8oKFomLfS4bXEpUUmAsOBS8Ifj42SpivoKeXcoMZqOwuWK54CHgfbTJbgJP brayan@DESKTOP-TRH75RO"
  node_desired_count = 3
  node_max_count = 5
}

eks_cluster_name = "otg_wp_cluster"
eks_cluster_version = "1.21"