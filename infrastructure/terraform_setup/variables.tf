variable "common_config" {
  type = object({
    aws_account_id = number
    region = string
    availability_zones = list(string)
  })
}

variable "terraform_state_bucket" {
  type = string
}

variable "terraform_lock_db" {
  type = string
}

variable "environment" {
  type = list(string)
}

