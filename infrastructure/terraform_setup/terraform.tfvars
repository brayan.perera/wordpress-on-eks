common_config = {
  aws_account_id = "763511508504"
  region = "us-east-1"
  availability_zones = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

terraform_state_bucket = "otgs-tf-state"
terraform_lock_db = "otgs-tf-lock"

environment = ["dev", "staging", "production"]