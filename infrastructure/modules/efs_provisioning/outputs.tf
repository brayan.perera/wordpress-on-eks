output "efs_dns_name" {
  value = aws_efs_file_system.efs.dns_name
}

output "efs_id" {
  value = aws_efs_file_system.efs.id
}

output "efs_ap_id" {
  value = aws_efs_access_point.efs_ap.id
}

output "efs_ap_arn" {
  value = aws_efs_access_point.efs_ap.arn
}

output "mount_target_ids" {
  value = aws_efs_mount_target.efs_mount_target.*.id
}

output "efs_mount_dns_names" {
  value = aws_efs_mount_target.efs_mount_target.*.dns_name
}

output "efs_mount_template" {
  value = data.template_file.amazon_linux_cloud_init_part
}