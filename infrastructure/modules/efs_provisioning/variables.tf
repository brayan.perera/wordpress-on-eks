variable "common_config" {
  type = object({
    aws_account_id = number
    region = string
    availability_zones = list(string)
    name_prefix = string
    environment = string
    git_lab_project_id = string
  })
}

variable "efs" {
  type = object({
    name = string
    subnets = list(string)
    security_groups = list(string)
    encrypted = bool
    creation_token = string
    mount_location = string
  })
}

variable "vpc_id" {
  type = string
}