resource "aws_efs_file_system" "efs" {
  creation_token = "eks-fs-${var.common_config.name_prefix}-${var.common_config.environment}-${var.efs.creation_token}"
  encrypted = var.efs.encrypted

  tags = {
    Name = "${var.efs.name}-${var.common_config.environment}"
    Environment = var.common_config.environment
  }
}

resource "aws_efs_mount_target" "efs_mount_target" {
  count = length(var.efs.subnets)
  file_system_id = aws_efs_file_system.efs.id
  subnet_id = var.efs.subnets[count.index]
  security_groups = var.efs.security_groups
}

resource "aws_efs_access_point" "efs_ap" {
  file_system_id = aws_efs_file_system.efs.id
}

resource "gitlab_project_variable" "var_efs_dns" {
   project   = var.common_config.git_lab_project_id
   key       = "${upper(var.common_config.environment)}_EFS_DNS"
   value     = aws_efs_file_system.efs.dns_name
   protected = true
   masked = false
   environment_scope = var.common_config.environment
}

resource "gitlab_project_variable" "var_efs_id" {
   project   = var.common_config.git_lab_project_id
   key       = "${upper(var.common_config.environment)}_EFS_ID"
   value     = aws_efs_file_system.efs.id
   protected = true
   masked = false
   environment_scope = var.common_config.environment
}

resource "gitlab_project_variable" "var_efs_ap_id" {
   project   = var.common_config.git_lab_project_id
   key       = "${upper(var.common_config.environment)}_EFS_AP_ID"
   value     = aws_efs_access_point.efs_ap.id
   protected = true
   masked = false
   environment_scope = var.common_config.environment
}

data "template_file" "amazon_linux_cloud_init_part" {
  template = <<EOL
# Install nfs-utils
cloud-init-per once yum_update yum update -y
cloud-init-per once install_nfs_utils yum install -y nfs-utils
# Create $${mount_location} folder
cloud-init-per once mkdir_efs mkdir $${mount_location}
# Mount $${mount_location}
cloud-init-per once mount_efs echo -e '$${efs_dns}:/ $${mount_location} nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0' >> /etc/fstab
mount -a
EOL
  vars = {
    efs_dns        = element(aws_efs_mount_target.efs_mount_target.*.dns_name, 0)
    mount_location = var.efs.mount_location
  }
}