locals {
  sec_group_rules = [
    {
      type = "ingress"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks  = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
      description = "Allow SSH to nodes"
    },
    {
      type = "ingress"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks  = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
      description = "Allow HTTP to nodes"
    },
    {
      type = "ingress"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks  = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
      description = "Allow HTTP to nodes"
    }
  ]
}

/* Create SSH Access Key */
resource "aws_key_pair" "eks_node_key" {
  key_name   = "eks_node_key_${var.common_config.environment}"
  public_key = var.ec2_launch_template.ssh_public_key
}

/* Create security group for access */
resource "aws_security_group" "eks_node_sg" {

  name        = "eks-node-${var.common_config.environment}-sg"
  description = "Security group to allow inbound to EKS Nodes"
  vpc_id      = var.vpc_id

  tags = {
    Name = "eks-node-${var.common_config.environment}-sg"
    Environment = var.common_config.environment
  }

  ingress = local.sec_group_rules

  egress = [
    {
      from_port         = 0
      to_port           = 0
      protocol          = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids = []
      security_groups = []
      self = false
      description = "All outgoing allow"
    }
  ]

}

resource "aws_security_group" "eks_worker_sg" {
  name_prefix = "worker_group_mgmt_one"
  vpc_id      = var.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [ var.vpc.vpc_cidr ]
  }
}

/* Create EKS Cluster and Nodes */

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version = "17.22.0"
  cluster_name    = var.eks.name
  cluster_version = var.eks.version
  vpc_id = var.vpc_id
  subnets = var.eks.subnets
  worker_additional_security_group_ids = concat(var.ec2_launch_template.vpc_security_group_ids, [aws_security_group.eks_node_sg.id])
  tags = {
    Environment = var.common_config.environment
  }

  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  worker_groups_launch_template = [
    {
      name                    = "wg-1"
      instance_type           = var.ec2_launch_template.instance_type
      asg_max_size            = var.ec2_launch_template.node_max_count
      asg_desired_capacity    = var.ec2_launch_template.node_desired_count
      public_ip               = true
      enable_monitoring       = true
      key_name                = aws_key_pair.eks_node_key.key_name
    }
  ]
}


/* Create EFS Drivers */
resource "aws_iam_policy" "policy" {
  name = "AmazonEKS_EFS_CSI_Driver_Policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "elasticfilesystem:DescribeAccessPoints",
        "elasticfilesystem:DescribeFileSystems"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "elasticfilesystem:CreateAccessPoint"
      ],
      "Resource": "*",
      "Condition": {
        "StringLike": {
          "aws:RequestTag/efs.csi.aws.com/cluster": "true"
        }
      }
    },
    {
      "Effect": "Allow",
      "Action": "elasticfilesystem:DeleteAccessPoint",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "aws:ResourceTag/efs.csi.aws.com/cluster": "true"
        }
      }
    }
  ]
}
EOF
}

locals {
  oidc_string_split = split("/", module.eks.cluster_oidc_issuer_url)
  cluster_identity_oidc_issuer = local.oidc_string_split[4]
}

resource "aws_iam_role" "csi_driver_role" {
  name = "AmazonEKS_EFS_CSI_DriverRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::${var.common_config.region}:oidc-provider/oidc.eks.${var.common_config.region}.amazonaws.com/id/${local.cluster_identity_oidc_issuer}"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "oidc.eks.${var.common_config.region}.amazonaws.com/id/${local.cluster_identity_oidc_issuer}:sub": "system:serviceaccount:kube-system:efs-csi-controller-sa"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole-attach" {
  role       = aws_iam_role.csi_driver_role.name
  policy_arn = aws_iam_policy.policy.arn
}