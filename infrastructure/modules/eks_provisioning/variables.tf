variable "common_config" {
  type = object({
    aws_account_id = number
    region = string
    availability_zones = list(string)
    name_prefix = string
    environment = string
    git_lab_project_id = string
  })
}

variable "eks" {
  type = object({
    name = string
    version = string
    subnets = list(string)
    security_groups = list(string)
  })
}

variable "ec2_launch_template" {
  type = object({
    name = string
    image_id = string
    instance_type = string
    ssh_public_key = string
    vpc_security_group_ids = list(string)
    node_desired_count = number
    node_max_count = number
  })
}

variable "vpc_id" {
  type = string
}

variable "vpc_security_group_id" {
  type = string
}

variable "vpc" {
  type = object({
    vpc_name = string
    vpc_cidr = string
    public_subnets_cidr = list(string)
    private_subnets_cidr = list(string)
  })
}