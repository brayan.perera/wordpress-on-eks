output "eks_cluster_id" {
  value = module.eks.cluster_id
}

output "eks_cluster_name" {
  value = var.eks.name
}

output "oidc_provider_arn" {
  value = module.eks.oidc_provider_arn
}

output "eks_sec_group_id" {
  value = module.eks.cluster_primary_security_group_id
}

output "key_name" {
  value = aws_key_pair.eks_node_key.key_name
}

output "node_sec_group_id" {
  value = aws_security_group.eks_node_sg.id
}