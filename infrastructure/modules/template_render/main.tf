locals {
  var_dict = {for index, x in var.var_list: x.key_name => x.value }
  rendered_content = templatefile("${path.module}/${var.template_file}", local.var_dict)
}

resource "null_resource" "file_render" {
  provisioner "local-exec" {
    command = "echo '${local.rendered_content}' > ${path.module}/${var.output_file}"
  }
}