variable "template_file" {
  type = string
}

variable "output_file" {
  type = string
}

variable "var_list" {
  type = list(object({
    key_name = string
    value = string
  }))
}