output "region" {
  value = var.common_config.region
}

output "cluster_name" {
  value = var.eks_cluster_name
}

output "oidc_provider_identity" {
  value = data.aws_eks_cluster.cluster.identity
}

output "efs_dns_name" {
  value = module.efs_provisioning.efs_dns_name
}

output "efs_id" {
  value = module.efs_provisioning.efs_id
}

output "efs_ap_id" {
  value = module.efs_provisioning.efs_ap_id
}

output "efs_ap_arn" {
  value = module.efs_provisioning.efs_ap_arn
}

output "efs_mount_target_ids" {
  value = module.efs_provisioning.mount_target_ids
}

output "efs_mount_template" {
  value = module.efs_provisioning.efs_mount_template
}