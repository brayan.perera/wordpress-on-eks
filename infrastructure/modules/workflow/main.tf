################################################################################
# VPC Creation
################################################################################

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.2.0"

  name                 = "${var.common_config.name_prefix}-${var.common_config.environment}-vpc"
  cidr                 = var.vpc.vpc_cidr
  azs                  = var.common_config.availability_zones
  private_subnets      = var.vpc.private_subnets_cidr
  public_subnets       = var.vpc.public_subnets_cidr
  enable_nat_gateway   = true
  one_nat_gateway_per_az  = true
  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    Name = "${var.common_config.environment}-vpc"
    Environment = var.common_config.environment
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb"  = "1"
    Environment = var.common_config.environment
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb" = "1"
    Environment = var.common_config.environment
  }
}

locals {
  eks = {
    name = var.eks_cluster_name
    version = var.eks_cluster_version
    subnets = module.vpc.public_subnets
    security_groups = [module.vpc.default_security_group_id]
  }

  ec2_launch_template = {
    vpc_security_group_ids = [module.vpc.default_security_group_id]
  }

  efs_data = {
    name = var.efs.name
    subnets = module.vpc.public_subnets
    security_groups = [module.vpc.default_security_group_id]
    encrypted = var.efs.encrypted
    creation_token = var.efs.creation_token
    mount_location = var.efs.mount_location
  }

}

################################################################################
# EKS Cluster Creation
################################################################################

module "eks_provisioning" {
  source = "../eks_provisioning"
  common_config = var.common_config
  ec2_launch_template = merge(var.ec2_launch_template, local.ec2_launch_template)
  eks = local.eks
  vpc_id = module.vpc.vpc_id
  vpc = var.vpc
  vpc_security_group_id = module.vpc.default_security_group_id
}

################################################################################
# Kubernetes provider configuration
################################################################################

data "aws_eks_cluster" "cluster" {
  name = module.eks_provisioning.eks_cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks_provisioning.eks_cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}



###############################################################################
# MySQL RDS Creation
###############################################################################

module "mysql_db_provisioning" {
  source = "../rds_db_provisioning"
  common_config = var.common_config
  db_availability_zones = var.common_config.availability_zones
  rds = var.rds
  security_groups = [module.vpc.default_security_group_id, module.eks_provisioning.eks_sec_group_id]
  subnets = module.vpc.private_subnets
}

################################################################################
# EFS Creation
################################################################################
module "efs_provisioning" {
  source = "../efs_provisioning"
  common_config = var.common_config
  efs = local.efs_data
  vpc_id = module.vpc.vpc_id
}

################################################################################
# Generate Wordpress Deployment config
################################################################################
module "kube_spec_file_generate" {
  source = "../template_render"
  count = length(local.template_data_list)
  output_file = local.template_data_list[count.index].file
  template_file = local.template_data_list[count.index].template
  var_list = local.template_data_list[count.index].vars
}


################################################################################
# Create Bastion Host
################################################################################
resource "aws_instance" "bastion" {
  instance_type = "t2.micro"
  ami = var.ec2_launch_template.image_id
  key_name = module.eks_provisioning.key_name
  subnet_id = module.vpc.public_subnets[0]
  security_groups = [module.vpc.default_security_group_id, module.eks_provisioning.eks_sec_group_id, module.eks_provisioning.node_sec_group_id]
  tags = {
    Name = "bastion-${var.common_config.environment}"
    Environment = var.common_config.environment
  }
}

module "ansible_play_file_generate" {
  source = "../template_render"
  template_file = "../../../ansible/templates/bastion_host_provision.yaml.j2"
  output_file = "../../../ansible/bastion_host_provision.yaml"
  var_list = [
    {
      key_name = "AWS_REGION"
      value = var.common_config.region
    },
    {
      key_name = "FILE_SYSTEM_ID"
      value = module.efs_provisioning.efs_id
    },
    {
      key_name = "MOUNT_DIR"
      value = var.efs.mount_location
    },
  ]
}