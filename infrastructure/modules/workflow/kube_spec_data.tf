locals {
  template_data_list = [
    {
      vars = [
        {
          key_name = "DB_PASSWORD"
          value = var.rds.db_pass
        }
      ]
      template = "../../../kubernetes_spec/templates/kustomization.yml.j2"
      file = "../../../kubernetes_spec/kustomization.yml"
    },
    {
      vars = [
        {
          key_name = "FILE_SYSTEM_ID"
          value = module.efs_provisioning.efs_id
        },
        {
          key_name = "FILE_SYSTEM_DNS"
          value = module.efs_provisioning.efs_dns_name
        },
        {
          key_name = "AWS_REGION"
          value = var.common_config.region
        }
      ]

      template = "../../../kubernetes_spec/templates/pv.yml.j2"
      file = "../../../kubernetes_spec/pv.yml"
    },
    {
      vars = [
        {
          key_name = "DB_USER"
          value = var.rds.db_user
        },
        {
          key_name = "DB_HOST"
          value = module.mysql_db_provisioning.rds_endpoint
        },
        {
          key_name = "DB_NAME"
          value = var.rds.db_name
        }
      ]
      template = "../../../kubernetes_spec/templates/deploy_wp.yml.j2"
      file = "../../../kubernetes_spec/deploy_wp.yml"
    },
    {
      vars = [
        {
          key_name = "AWS_ACCOUNT_ID"
          value = var.common_config.aws_account_id
        }
      ]
      template = "../../../kubernetes_spec/templates/service_account.yml.j2"
      file = "../../../kubernetes_spec/service_account.yml"
    }
  ]
}