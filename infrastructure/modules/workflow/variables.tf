variable "common_config" {
  type = object({
    aws_account_id = number
    region = string
    availability_zones = list(string)
    environment = string
    name_prefix = string
    git_lab_project_id = string
  })
}

variable "vpc" {
  type = object({
    vpc_name = string
    vpc_cidr = string
    public_subnets_cidr = list(string)
    private_subnets_cidr = list(string)
  })
}

variable "rds" {
  type = object({
    instance_name = string
    instance_class = string
    allocated_storage = number
    storage_type = string
    engine = string
    engine_version = string
    family = string
    db_name = string
    db_user = string
    db_pass = string
    backup_retention_period = number
    backup_window = string
    maintenance_window = string
  })
}

variable "efs" {
  type = object({
    name: string
    encrypted = bool
    creation_token = string
    mount_location = string
  })
}

variable "eks_cluster_name" {
  type = string
}

variable "eks_cluster_version" {
  type = string
}

variable "ec2_launch_template" {
  type = object({
    name = string
    image_id = string
    instance_type = string
    ssh_public_key = string
    node_desired_count = number
    node_max_count = number
  })
}