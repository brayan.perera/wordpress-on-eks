output "rds_cluster_id" {
  value = aws_db_instance.rds_db.id
}

output "rds_endpoint" {
  value = aws_db_instance.rds_db.endpoint
}