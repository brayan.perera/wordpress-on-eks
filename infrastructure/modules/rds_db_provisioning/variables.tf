variable "db_availability_zones" {
  type = list(string)
}

variable "common_config" {
  type = object({
    aws_account_id = number
    region = string
    availability_zones = list(string)
    name_prefix = string
    environment = string
    git_lab_project_id = string
  })
}

variable "subnets" {
  type = list(string)
  description = "VPC Subnets"
}

variable "security_groups" {
  type = list(string)
  description = "VPC Security groups"
}

variable "rds" {
  type = object({
    instance_name = string
    instance_class = string
    allocated_storage = number
    storage_type = string
    engine = string
    engine_version = string
    family = string
    db_name = string
    db_user = string
    db_pass = string
    backup_retention_period = number
    backup_window = string
    maintenance_window = string
  })
}