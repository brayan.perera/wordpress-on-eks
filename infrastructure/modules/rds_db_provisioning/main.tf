resource "aws_db_subnet_group" "rds_subnet" {
  name       = "rds_subnet_${var.common_config.environment}"
  subnet_ids = var.subnets

  tags = {
    Name = "${var.rds.engine} DB Access subnets"
    Environment = var.common_config.environment
  }
}

resource "aws_db_parameter_group" "mysql_db_pg" {
  name   = "${var.rds.engine}-db-pg-${var.common_config.environment}"
  family = var.rds.family
}

resource "aws_db_option_group" "mysql_db_og" {
  name = "${var.rds.engine}-db-og-${var.common_config.environment}"
  engine_name          = var.rds.engine
  major_engine_version = var.rds.engine_version
}

resource "aws_db_instance" "rds_db" {
  identifier             = var.rds.instance_name
  instance_class         = var.rds.instance_class
  allocated_storage      = var.rds.allocated_storage
  engine                 = var.rds.engine
  engine_version         = var.rds.engine_version
  name                   = var.rds.db_name
  username               = var.rds.db_user
  password               = var.rds.db_pass
  db_subnet_group_name   = aws_db_subnet_group.rds_subnet.name
  vpc_security_group_ids = var.security_groups
  parameter_group_name   = aws_db_parameter_group.mysql_db_pg.name
  option_group_name = aws_db_option_group.mysql_db_og.name
  skip_final_snapshot    = true
  backup_retention_period = var.rds.backup_retention_period
  backup_window     = var.rds.backup_window
  maintenance_window = var.rds.maintenance_window
  multi_az = true

  tags = {
    Name = var.rds.instance_name
    Environment = var.common_config.environment
  }
}

locals {
  rds_endpoint = split(":",  aws_db_instance.rds_db.endpoint)
}

resource "gitlab_project_variable" "var_rds_endpoint" {
   project   = var.common_config.git_lab_project_id
   key       = "${upper(var.common_config.environment)}_RDS_ENDPOINT"
   value     = local.rds_endpoint[0]
   protected = true
   masked = false
   environment_scope = var.common_config.environment
}