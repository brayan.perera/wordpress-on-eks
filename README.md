# wordpress-on-eks

## Overview 

This project contains the sample IaC for deploy VPC, RDS, EKS Cluster and EFS for deploy 
wordpress application in AWS environment. 

### Pre-Requisites 
- AWS Account
  - IAM user which have access to create IAM, S3, EKS, EC2, ELB, EFS and RDS resources
- GitLab Projects
  - Project to hold Infrastructure provisioning code
    - GitLab Project Variables 
      - `AWS_ACCESS_KEY_ID`: To store AWS Access key for terraform execution via GitLab Pipelines
      - `AWS_SECRET_ACCESS_KEY`: Store AWS Secret key
      - `TF_PAT`: Personal Access Token of GitLab project to provision Environment Variables from Terraform Workflow
  - Project to hold Wordpress Site files

- Instance with terraform installed for initial setup

## Architecture

## Getting Started

### Initial Setup
- Checkout IaC project to Terraform instance
  ```
  git clone git@gitlab.com:brayan.perera/wordpress-on-eks.git
  ```
  
- Setup Terraform state management via S3 and Dynamodb
  ```
  cd infrastructure/terraform_setup
  terraform init
  terraform plan
  terraform apply
  ```
  > This will create Terraform state management resources in S3 and Dynamodb for dev, staging and production environments

- Create gitlab CICD Pipeline spec and commit
  ```
  cp pipeline/gitlab-ci.yml .gitlab-ci.yml
  git commit -m "Add TF state and add pipeline"
  git push
  ```

<br/>

### Manual Execution

- Switch to the env directory
````
cd infrastructure/dev/
````

- Configure `terraform.tfvars`
```
vi terraform.tfvars
```
- Perform Terraform execution
````
export AWS_ACCESS_KEY_ID=""
export AWS_SECRET_ACCESS_KEY=""
export AWS_DEFAULT_REGION="us-east-1"
export GITLAB_TOKEN=""
export GITLAB_BASE_URL="https://gitlab.com/api/v4/"

terraform init
terraform plan
terraform apply								
````

- Configure kubectl
````
aws eks --region $(terraform output region) update-kubeconfig --name $(terraform output cluster_name)

kubectl apply -f kubernetes_spec/service_account.yml
````

- Add efs driver to eks cluster
```
kubectl apply -k "github.com/kubernetes-sigs/aws-efs-csi-driver/deploy/kubernetes/overlays/stable/?ref=release-1.3"
```

- Perform Deployment create
```
cd kubernetes_spec/

kubectl apply -k .
```



### Destroying an environment

To destroy an environment, we are able to use terraform destroy. 

- Take a pull of IaC project in terraform instance
  - E.g: dev environment
  ```
  cd <path to project git location>
  git pull
  ```
  
- Switch to env dir and perform destroy
  ```
  cd infrastructure/dev
  terraform destroy
  ```
